import math
from abc import ABCMeta

from hard.interrupt import IntCodes
from soft.memory import PageTableEntry
from usr.logger import Logger, LogCodes


class MemoryLoader(metaclass=ABCMeta):
    def __init__(self, disk, irq_table):
        self.disk = disk
        self.irq_table = irq_table
        self.kernel = None

    def set_kernel(self, kernel):
        self.kernel = kernel

    def load(self, filename, args):
        Logger.log(LogCodes.PROGRAM_LDR, f'path: {filename}')
        program = self.disk.get_file(filename)
        if program is None:
            return None, None

        if program.param_count != len(args):
            self.irq_table.rise(IntCodes.ARG_MISMATCH, filename, program.param_count)
            return None, None

        return program, program.get_size()


class MemoryLoaderContiguous(MemoryLoader):
    def __init__(self, disk, irq_table):
        super().__init__(disk, irq_table)

    # noinspection PyUnresolvedReferences
    def load(self, filename, args):
        program, program_size = super().load(filename, args)
        if program is None:
            return None, None, None

        base = self.kernel.memory_manager.find_free_block(program_size)

        if base is None:
            self.irq_table.rise(IntCodes.NO_MEM_SPACE, filename, program_size)
            return None, None, None

        self.kernel.memory_manager.mmu.base_address = base

        # load program
        for i in range(0, program_size):
            self.kernel.memory_manager.set_cell(i, program.code[i])

        return base, program.start_at, program_size


class MemoryLoaderPaged(MemoryLoader):
    def __init__(self, disk, irq_table):
        super().__init__(disk, irq_table)

    def load(self, filename, args):
        program, program_size = super().load(filename, args)
        if program is None:
            return None, None, None

        pages_count = math.ceil(program_size / self.kernel.memory_manager.mmu.frame_size)

        return pages_count, program.start_at, program_size

    def load_page(self, pid, page, storage):
        if storage == PageTableEntry.NOT_ALLOCATED:
            self.load_page_from_disk_image(pid, page)
        elif storage == PageTableEntry.IN_SWAP:
            self.load_page_from_swap_file(pid, page)

    def load_page_from_disk_image(self, pid, page):
        filename = self.kernel.pcb_table.get_pcb(pid).path
        frame_size = self.kernel.memory_manager.mmu.frame_size
        start = page * frame_size
        program_slice = self.get_program_slice_from_disk_image(filename, start)

        target_frame = self.get_target_frame(pid, filename)

        self.kernel.memory_manager.assign_frame(pid, page, target_frame)

        # load program
        self.load_slice(target_frame, program_slice)

        # load arguments
        self.load_arguments(target_frame, pid, page)

    def get_target_frame(self, pid, filename):
        target_frame = self.kernel.memory_manager.get_free_frame()

        if target_frame is not None:
            return target_frame

        target_frame, target_frame_entry = self.kernel.memory_manager.choose_frame_for_eviction()
        if self.evict_frame(pid, filename, target_frame, target_frame_entry):
            return target_frame
        else:
            return False

    def evict_frame(self, req_pid, req_filename, target_frame, target_frame_entry):
        target_block = self.kernel.swap.get_free_block()
        if target_block is None:
            self.irq_table.rise(IntCodes.NO_SWAP_SPACE, req_pid, req_filename)
            return False

        self.kernel.swap.swap_out(target_frame, target_block)
        self.kernel.memory_manager.assign_swap_block(target_frame_entry.pid, target_frame_entry.page,
                                                     target_frame, target_block)
        return target_block is not None

    def get_program_slice_from_disk_image(self, filename, start):
        count = self.kernel.memory_manager.mmu.frame_size
        program = self.disk.get_file(filename)
        if program is None:
            return None
        return program.code[start:start+count]

    def load_arguments(self, target_frame, pid, page):
        args = self.kernel.memory_manager.cli_arguments.get(pid)
        if (page != 0) or (args is None):
            return
        self.load_slice(target_frame, args)

        del (self.kernel.memory_manager.cli_arguments[pid])

    def load_slice(self, target_frame, p_slice):
        for i in range(0, len(p_slice)):
            self.kernel.memory_manager.mmu.set_cell((target_frame, i), p_slice[i])

    def load_page_from_swap_file(self, pid, page):
        filename = self.kernel.pcb_table.get_pcb(pid).path
        target_frame = self.get_target_frame(pid, filename)
        block_to_read = self.kernel.memory_manager.get_block(pid, page)

        self.kernel.swap.swap_in(block_to_read, target_frame)

        self.kernel.memory_manager.assign_frame(pid, page, target_frame)
