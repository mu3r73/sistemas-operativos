from soft.pcb import PCBState
from usr.logger import Logger, LogCodes


class DeviceManager:
    def __init__(self):
        # self.kernel = None
        self.devices = {}  # device_id -> logical_device

    # def set_kernel(self, kernel):
    #     self.kernel = kernel

    def add_device(self, logical_device):
        self.devices[logical_device.device.id] = logical_device
        logical_device.set_device_manager(self)

    def add_job(self, dev_id, pcb, data):
        self.devices[dev_id].add_job(pcb, data)

    def on_tick(self):
        for logical_device in self.devices.values():
            logical_device.on_tick()

    def on_log(self):
        Logger.log(LogCodes.DEV_MAN, f'{self.as_text()}')

    def as_text(self):
        return '\n'.join([d.as_text() for d in self.devices.values()])


class LogicalDevice:
    def __init__(self, device):
        self.device_manager = None
        self.device = device
        self.servicing = None
        self.waiting = []

    def set_device_manager(self, device_manager):
        self.device_manager = device_manager

    def add_job(self, pcb, data):
        pcb.state = PCBState.WAITING
        if self.servicing:
            Logger.log(LogCodes.LOGICAL_DEV, f'added to queue: {pcb.pid}')
            Logger.log(LogCodes.LOGICAL_DEV_DET, f'data: {data}')
            self.waiting.append(Job(pcb, data))
            Logger.log(LogCodes.LOGICAL_DEV_DET, f'waiting: {self.waiting_as_text()}')
        else:
            Logger.log(LogCodes.LOGICAL_DEV, f'performing I/O - pid: {pcb.pid}')
            Logger.log(LogCodes.LOGICAL_DEV_DET, f'data: {data}')
            self.servicing = pcb
            self.device.data = data
        self.device_manager.on_log()

    def on_tick(self):
        self.update_jobs()
        if self.servicing:
            self.device.on_tick()

    def update_jobs(self):
        if self.servicing:
            self.servicing.inc_io_time()
        for job in self.waiting:
            job.pcb.inc_io_time()

    def serve_next(self):
        next_job = self.choose_next_job()
        if next_job:
            self.servicing = next_job.pcb
            self.device.data = next_job.data
            Logger.log(LogCodes.LOGICAL_DEV, f'next I/O job - {next_job.pcb.as_text()}')
            Logger.log(LogCodes.LOGICAL_DEV_DET, f'data: {self.device.data}')
            Logger.log(LogCodes.LOGICAL_DEV_DET, f'waiting: {self.waiting_as_text()}')
        else:
            self.servicing = None
        self.device_manager.on_log()

    def choose_next_job(self):
        try:
            next_job = self.waiting.pop(0)
            while next_job.pcb.state == PCBState.TERMINATED:
                next_job = self.waiting.pop(0)
        except IndexError:
            next_job = None
        return next_job

    def as_text(self):
        return f'id: {self.device.id}, servicing: {self.servicing_as_text()}, waiting: {self.waiting_as_text()}'

    def servicing_as_text(self):
        if self.servicing:
            return self.servicing.as_text()
        else:
            return 'empty'

    def waiting_as_text(self):
        if self.waiting:
            return '; '.join([job.pcb.as_text() for job in self.waiting])
        else:
            return 'empty'


class Job:
    def __init__(self, pcb, data):
        self.pcb = pcb
        self.data = data
