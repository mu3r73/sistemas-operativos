from soft.pcb import PCBState
from usr.logger import Logger, LogCodes
from usr.program import Program


class Dispatcher:
    def __init__(self, clock, cpu):
        self.clock = clock
        self.cpu = cpu
        self.running = None
        self.kernel = None

    def set_kernel(self, kernel):
        self.kernel = kernel

    def is_cpu_idle(self):
        return self.cpu.is_idle()

    def is_running_process(self, pid):
        return self.running and (self.running.pid == pid)

    def dispatch(self, pcb):
        if pcb:
            Logger.log(LogCodes.DISPATCH_DET, f'pid: {pcb.pid}')
            self.running = pcb
            self.load_state(pcb)
            self.running.state = PCBState.RUNNING
            self.kernel.pcb_table.on_log()
            Logger.log(LogCodes.DISPATCH, f'{self.running.as_text()}')
            if self.kernel.has_round_robin_timer():
                self.kernel.round_robin_timer.reset()

    def on_kill(self):
        self.running.end_time = self.clock.ticks
        self.interrupt(PCBState.TERMINATED)

    def on_pause(self):
        self.interrupt(PCBState.READY)

    def wait_for_io(self):
        self.interrupt(PCBState.WAITING)

    def interrupt(self, pcb_state):
        Logger.log(LogCodes.DISPATCH_DET, f'state: {pcb_state}')
        pcb = self.running
        self.running = None
        # pcb = self.save_state(pcb)
        self.save_state(pcb)
        pcb.state = pcb_state
        self.cpu.pc = -1  # idle
        self.kernel.pcb_table.on_log()
        Logger.log(LogCodes.DISPATCH, 'empty')

    def save_state(self, pcb=None):
        if not self.is_cpu_idle():
            pcb = pcb or self.running
            pcb.pc = self.cpu.pc
            pcb.reg_a = self.cpu.reg_a
            pcb.reg_b = self.cpu.reg_b
            pcb.reg_x = self.cpu.reg_x
        return pcb

    def load_state(self, pcb):
        if pcb is not None:
            self.kernel.memory_manager.switch_context_to(pcb.pid)
            self.cpu.pc = pcb.pc
            self.cpu.reg_a = pcb.reg_a
            self.cpu.reg_b = pcb.reg_b
            self.cpu.reg_x = pcb.reg_x
            Logger.log(LogCodes.DISPATCH_SWITCH, self.kernel.hardware.disk.get_file(pcb.path).as_source())

    def on_tick(self):
        if self.running:
            self.running.inc_cpu_time()
        self.log_status()

    def log_status(self):
        Logger.log(LogCodes.STATS, self.kernel.pcb_table.get_global_stats(self.kernel.memory_manager))
