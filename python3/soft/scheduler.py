from abc import ABCMeta, abstractmethod

# thread safe
from queue import PriorityQueue, Empty

from soft.pcb import PCBState
from usr.logger import Logger, LogCodes


class Scheduler:
    def __init__(self, kernel):
        self.kernel = kernel
        self.algorithm = None  # SchedulingAlgorithm

    def set_algorithm(self, algorithm):
        self.algorithm = algorithm
        # TODO: allow PCB queue transfer between algorithms

    def schedule(self, pcb):
        if self.kernel.dispatcher.is_cpu_idle():
            self.kernel.dispatcher.dispatch(pcb)
        else:
            self.algorithm.schedule(pcb)

    def choose_next_pcb(self):
        return self.algorithm.choose_next_pcb()


class SchedulingAlgorithm(metaclass=ABCMeta):
    def __init__(self, kernel):
        self.kernel = kernel

    def schedule(self, pcb):
        if pcb:
            pcb.state = PCBState.READY
            self.add_to_ready(pcb)
            Logger.log(LogCodes.SCHEDULE, f'added to ready: {pcb.pid}')
            Logger.log(LogCodes.SCHEDULE_DET, f'ready: [{self.ready_as_text()}]')

    @abstractmethod
    def add_to_ready(self, pcb):
        pass

    def choose_next_pcb(self):
        next_pcb = self.do_choose_next_pcb()
        if next_pcb:
            Logger.log(LogCodes.SCHEDULE, f'next_pid: {next_pcb.pid}')
        Logger.log(LogCodes.SCHEDULE_DET, f'ready: [{self.ready_as_text()}]')
        return next_pcb

    @abstractmethod
    def do_choose_next_pcb(self):
        pass

    @abstractmethod
    def is_ready_empty(self):
        pass

    def ready_as_text(self):
        if self.is_ready_empty():
            return 'empty'
        else:
            # noinspection PyTypeChecker
            return '; '.join([pcb.as_text() for pcb in self.ready_as_list()])

    @abstractmethod
    def ready_as_list(self):
        pass


class SchedulingFCFS(SchedulingAlgorithm):
    def __init__(self, kernel):
        super().__init__(kernel)
        self.ready = []

    def is_ready_empty(self):
        return not self.ready

    def add_to_ready(self, pcb):
        self.ready.append(pcb)

    def do_choose_next_pcb(self):
        try:
            next_pcb = self.ready.pop(0)
            while next_pcb.state == PCBState.TERMINATED:
                next_pcb = self.ready.pop(0)
        except IndexError:
            next_pcb = None
        return next_pcb

    def ready_as_list(self):
        return self.ready


class SchedulingPriority(SchedulingAlgorithm):
    # priority - 0: highest - 9: lowest
    def __init__(self, kernel):
        super().__init__(kernel)
        self.ready = PriorityQueue()

    def is_ready_empty(self):
        return not self.ready.qsize()

    def add_to_ready(self, pcb):
        self.ready.put_nowait(pcb)

    def do_choose_next_pcb(self):
        try:
            next_pcb = self.ready.get_nowait()
            while next_pcb.state == PCBState.TERMINATED:
                next_pcb = self.ready.get_nowait()
        except Empty:
            next_pcb = None
        return next_pcb

    def ready_as_list(self):
        return self.ready.queue


class SchedulingPriorityPreemptive(SchedulingPriority):
    def schedule(self, pcb):
        running_pcb = self.kernel.dispatcher.running
        if SchedulingPriorityPreemptive.should_preempt(pcb, running_pcb):
            self.kernel.dispatcher.on_pause()
            super().schedule(running_pcb)
            self.kernel.dispatcher.dispatch(pcb)
        else:
            super().schedule(pcb)

    @staticmethod
    def should_preempt(pcb, running_pcb):
        return pcb and (pcb < running_pcb)
