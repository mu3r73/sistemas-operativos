from usr.logger import Logger, LogCodes


class Swap:
    def __init__(self, mmu, disk, multiplier):
        self.mmu = mmu
        self.disk = disk
        self.block_size = self.mmu.frame_size
        self.block_count = self.mmu.frame_count * multiplier
        self.free_blocks = [True] * self.block_count
        self.free_blocks_count = self.block_count
        self.create_swap_file()

    def create_swap_file(self):
        swap_file = SwapFile(self.block_size, self.block_count)
        self.disk.write_file('swap', swap_file)

    def get_free_block(self):
        return self.free_blocks.index(True)

    def is_valid_block(self, block):
        return (block is not None) and (block >= 0) and (block < self.block_count)

    def swap_out(self, frame, block):
        # write frame to block
        swap_file = self.disk.get_file('swap')
        block_address = block * self.block_size
        for i in range(0, self.block_size):
            swap_file.data[block_address + i] = self.mmu.get_cell((frame, i))

        self.free_blocks[block] = False
        self.free_blocks_count -= 1

    def swap_in(self, block, frame):
        # read block to frame
        swap_file = self.disk.get_file('swap')
        block_address = block * self.block_size
        for i in range(0, self.block_size):
            self.mmu.set_cell((frame, i), swap_file.data[block_address + i])

        self.release_block(block)

    def release_block(self, block):
        self.free_blocks[block] = True
        self.free_blocks_count += 1

    def as_text(self):
        return self.disk.get_file('swap').as_text()

    def get_totals(self):
        return f'swap - total: {self.block_count * self.block_size}, ' \
               f'free: {self.free_blocks_count * self.block_size}'


class SwapFile:
    def __init__(self, block_size, block_count):
        self.block_size = block_size
        self.block_count = block_count
        self.data = [None] * self.block_size * self.block_count
        Logger.log(LogCodes.SYS_INFO, f'swap file - size: {block_count * block_size} ({block_count} blocks)')

    def get_size(self):
        return self.block_size * self.block_count

    def as_text(self, _=None):
        columns = self.block_size
        output = []
        for x in self.data:
            output.append(x or 0)
        output = [f'b{x//columns}: {output[x:x+columns]}'
                  for x in range(0, len(output), columns)]
        return '\n'.join(output)
