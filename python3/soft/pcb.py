from functools import total_ordering

from usr.logger import Logger, LogCodes


class PCBTable:
    def __init__(self, clock):
        self.table = {}  # pid -> pcb
        self.next_pid = 0
        self.clock = clock

    def create_pcb(self, path, priority, start_at, size):
        new_pcb = PCB(self.next_pid, path, priority, start_at, size, self.clock.ticks)
        Logger.log(LogCodes.PCB_TABLE_DET, f'new_pcb: [{new_pcb.as_text()}]')
        self.table[self.next_pid] = new_pcb
        Logger.log(LogCodes.PCB_TABLE_DET, f'pcb_table: [{self.as_text()}]')
        self.next_pid += 1
        return new_pcb

    def get_pcb(self, pid):
        return self.table.get(pid)

    def on_kill(self, pid):
        if pid in self.table.keys():
            self.table[pid].state = PCBState.TERMINATED
            Logger.log(LogCodes.PCB_TABLE, f'killed: {pid}')
            Logger.log(LogCodes.PCB_TABLE_DET, f'pcb_table: [{self.as_text()}]')
            self.on_log()

    def on_log(self):
        return Logger.log(LogCodes.PCB_TABLE, f'{self.as_text()}')

    def as_text(self):
        if self.table.values():
            return '\n'.join([pcb.as_text() for pcb in self.table.values()])
        else:
            return 'empty'

    def as_gantt_data(self):
        return [(pcb.pid, pcb.as_gantt_char()) for pcb in self.table.values()]

    def pcb_as_text(self, pid):
        pcb = self.get_pcb(pid)
        if pcb:
            return pcb.as_text()
        else:
            return 'no such process'

    def get_global_stats(self, memory_manager):
        if self.table.values():
            return '\n'.join([self.get_stats(pcb, memory_manager) for pcb in self.table.values()])
        else:
            return 'empty'

    def get_stats(self, pcb, memory_manager):
        if pcb:
            elapsed = self.get_elapsed_time(pcb)
            cpu_time_p, io_time_p = '?', '?'
            if elapsed > 0:
                cpu_time_p = f'{(pcb.cpu_time * 100 / elapsed):.1f}'
                io_time_p = f'{(pcb.io_time * 100 / elapsed):.1f}'
            return f'{pcb.pid}({pcb.path}) - {memory_manager.get_stats(pcb.pid)}, ' \
                   f'state: {PCBState.states[pcb.state]},\n    ' \
                   f'time: [ start: {pcb.start_time}, end: {pcb.get_end_time()}, ' \
                   f'total: {elapsed}, ' \
                   f'cpu: {pcb.cpu_time} ({cpu_time_p}%) - ' \
                   f'i/o: {pcb.io_time} ({io_time_p}%) ]'
        else:
            return 'no such process'

    def get_elapsed_time(self, pcb):
        return (pcb.end_time or self.clock.ticks) - pcb.start_time


@total_ordering
class PCB:
    # class const
    DEFAULT_PRIORITY = 5  # 0: highest - 9: lowest

    def __init__(self, pid, path, priority, start_at, size, start_time):
        self.pid = pid
        self.path = path
        self.priority = priority
        self.start_at = start_at
        self.size = size
        self.pc = start_at
        self.reg_a = None
        self.reg_b = None
        self.reg_x = None
        self.state = PCBState.NEW
        self.start_time = start_time
        self.end_time = None
        self.cpu_time = 0
        self.io_time = 0

    # __eq__ and __lt__ needed for PriorityQueue
    def __eq__(self, other):
        return self.pid == other.pid

    def __lt__(self, other):
        return (self.priority < other.priority) or ((self.priority == other.priority) and (self.pid < other.pid))

    def inc_cpu_time(self):
        if not self.state == PCBState.TERMINATED:
            self.cpu_time += 1

    def inc_io_time(self):
        if not self.state == PCBState.TERMINATED:
            self.io_time += 1

    def as_text(self):
        return f'pid: {self.pid}, path: {self.path}, priority: {self.priority}.' \
               f'pc: {self.pc}, state: {PCBState.states[self.state]}'

    def as_gantt_char(self):
        return PCBState.gantt_chars[self.state]

    def get_end_time(self):
        return self.end_time or '?'


class PCBState:
    # class const
    NEW = 0
    READY = 1
    RUNNING = 2
    WAITING = 3
    TERMINATED = 4

    states = {
        NEW: 'NEW',
        READY: 'READY',
        RUNNING: 'RUNNING',
        WAITING: 'WAITING',
        TERMINATED: 'TERMINATED',
    }

    gantt_chars = {
        NEW: ' ',
        READY: '.',
        RUNNING: '*',
        WAITING: '|',
        TERMINATED: '-',
    }


class CPUState:
    def __init__(self, reg_a, reg_b, reg_x):
        self.reg_a = reg_a
        self.reg_b = reg_b
        self.reg_x = reg_x
