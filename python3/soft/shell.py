from hard.interrupt import IntCodes
from soft.pcb import PCB
from usr.logger import Logger, LogCodes


class Shell:
    def __init__(self, irq_table):
        self.irq_table = irq_table
        self.kernel = None

    def set_kernel(self, kernel):
        self.kernel = kernel

    @staticmethod
    def is_valid_command(cmd):
        return cmd in ShellCommands.commands

    def run(self, *cmd_line):
        Logger.log(LogCodes.SHELL, f'\n> {" ".join(cmd_line)}')
        cmd, args = Shell.parse_cmd_line(cmd_line)
        if not Shell.is_valid_command(cmd):
            Logger.log(LogCodes.SHELL, f'invalid command: {cmd}')
            return
        self.interpret(cmd, args)

    @staticmethod
    def parse_cmd_line(cmd_line):
        if not cmd_line[0]:  # nothing to do
            Logger.log(LogCodes.SHELL, f'commands: {Shell.list_commands_as_text()}')
            return None, None

        cl_parsed = cmd_line[0].split()
        cmd = cl_parsed[0]
        args = cl_parsed[1:]

        return cmd, args

    def interpret(self, cmd, args):
        if cmd == 'help':
            Shell.show_help()
        elif (cmd == 'dir') or (cmd == 'ls'):
            self.run_ls(args)
        elif cmd == 'free':
            self.run_free()
        elif cmd == 'memdump':
            self.run_memdump()
        elif cmd == 'objdump':
            self.run_objdump(args)
        elif cmd == 'ps':
            self.run_ps(args)
        elif cmd == 'run':
            self.run_from_disk(args)
        elif cmd == 'kill':
            self.run_kill(args)
        elif cmd == 'stats':
            self.run_stats(args)
        elif cmd == 'swapdump':
            self.run_swapdump()

    @staticmethod
    def show_help():
        output = f'commands: {Shell.list_commands_as_text()}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    @staticmethod
    def list_commands_as_text():
        return ", ".join(ShellCommands.commands)

    def run_ls(self, args):
        if not args:
            output = f'files: {self.kernel.hardware.disk.as_text()}'
        else:
            path = args[0]
            output = f'file: {self.kernel.hardware.disk.program_as_text(path)}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_free(self):
        output = f'memory - {self.kernel.memory_manager.get_totals()}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_ps(self, args):
        if not args:
            output = f'pcb table:\n{self.kernel.pcb_table.as_text()}'
        else:
            pid = int(args[0])
            output = f'pcb - {self.kernel.pcb_table.pcb_as_text(pid)}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_from_disk(self, cmd):
        if not cmd:
            output = 'syntax: run <file_name>'
            Logger.log(LogCodes.SHELL, output)
            print(output)
        else:
            path, priority, args = Shell.parse_args(cmd)
            self.irq_table.rise(IntCodes.NEW_PROCESS, path, priority, args)

    def run_kill(self, args):
        if (args is None) or (len(args) == 0) or (not args[0].isnumeric()):
            output = 'syntax: kill <pid>'
        else:
            pid = int(args[0])
            self.irq_table.rise(IntCodes.KILL_PROCESS, pid)
            output = f'pcb table:\n{self.kernel.pcb_table.as_text()}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_stats(self, args):
        if not args:
            output = f'global stats:\n{self.kernel.pcb_table.get_global_stats(self.kernel.memory_manager)}'
        else:
            pid = int(args[0])
            pcb = self.kernel.pcb_table.get_pcb(pid)
            output = f'pcb: {self.kernel.pcb_table.get_stats(pcb, self.kernel.memory_manager)}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_memdump(self):
        output = f'memory dump:\n{self.kernel.hardware.mmu.as_text()}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_objdump(self, args=()):
        output = 'syntax: objdump [d] <file_name>'
        opt, path, file = None, None, None

        if len(args) == 1:
            path = args[0]
        elif len(args) == 2:
            opt, path = args[0], args[1]

        if (opt is not None) and (opt != 'd'):
            output = f'invalid option\n{output}'
        elif path is not None:
            file = self.kernel.hardware.disk.get_file(path)

        if file is not None:
            if (opt == 'd') and hasattr(file, 'code'):
                output = file.as_source()
            else:
                columns = self.kernel.hardware.mmu.frame_size \
                    if hasattr(self.kernel.hardware.mmu, 'frame_size') else 4
                output = file.as_text(columns)

        Logger.log(LogCodes.SHELL, output)
        print(output)

    def run_swapdump(self):
        output = f'swap dump:\n{self.kernel.swap.as_text()}'
        Logger.log(LogCodes.SHELL, output)
        print(output)

    @staticmethod
    def parse_args(cmd_args):
        if cmd_args[0].isnumeric():
            priority = int(cmd_args[0])
            path = cmd_args[1]
            args = tuple(cmd_args[2:])
        else:
            priority = PCB.DEFAULT_PRIORITY
            path = cmd_args[0]
            args = tuple(cmd_args[1:])
        args = [int(arg) for arg in args if arg.isnumeric()]
        return path, priority, args


class ShellCommands:
    # class vars
    commands = ['dir', 'help', 'free', 'kill', 'ls', 'memdump', 'objdump', 'ps', 'run', 'stats', 'swapdump']
