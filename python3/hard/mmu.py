import math

from abc import ABCMeta, abstractmethod

from hard.interrupt import IntCodes


class MMU(metaclass=ABCMeta):
    def __init__(self, memory, irq_table):
        self.memory = memory
        self.irq_table = irq_table

    @abstractmethod
    def get_size(self):
        pass

    @abstractmethod
    def get_cell(self, address):
        pass

    @abstractmethod
    def set_cell(self, address, value):
        pass

    @abstractmethod
    def get_absolute_address(self, address):
        pass

    def is_in_range(self, absolute_address):
        return (absolute_address >= 0) and (absolute_address < self.memory.size)

    @abstractmethod
    def as_text(self):
        pass


class MMUContiguous(MMU):
    def __init__(self, memory, irq_table, _):
        super().__init__(memory, irq_table)
        self.base_address = 0

    def get_size(self):
        return self.memory.size

    def get_cell(self, relative_address):
        absolute_address = self.get_absolute_address(relative_address)
        if not self.is_in_range(absolute_address):
            self.irq_table.rise(IntCodes.INV_ADDRESS)
        value = self.memory.get_cell(absolute_address)
        return value

    def set_cell(self, relative_address, value):
        absolute_address = self.get_absolute_address(relative_address)
        if not self.is_in_range(absolute_address):
            self.irq_table.rise(IntCodes.INV_ADDRESS)
        self.memory.set_cell(absolute_address, value)

    def get_absolute_address(self, relative_address):
        return self.base_address + relative_address

    def as_text(self):
        output = []
        for i in range(0, self.get_size()):
            output.append(self.memory.get_cell(i) or 0)
        output = [f'{x}: {output[x:x+8]}' for x in range(0, len(output), 8)]
        return '\n'.join(''.join(map(str, sl)) for sl in output)


class MMUPaged(MMU):
    def __init__(self, memory, irq_table, frame_size):
        super().__init__(memory, irq_table)
        self.frame_size = frame_size
        self.frame_count = math.ceil(memory.size / frame_size)

    def get_size(self):
        return self.frame_count * self.frame_size

    def get_cell(self, address):
        return self.memory.get_cell(self.get_absolute_address(address))

    def set_cell(self, address, value):
        self.memory.set_cell(self.get_absolute_address(address), value)

    def get_absolute_address(self, address):
        frame, offset = address[0], address[1]
        if not self.is_valid_frame(frame) or not self.is_valid_offset(offset):
            self.irq_table.rise(IntCodes.INV_ADDRESS)
            return None

        absolute_address = frame * self.frame_size + offset
        if not self.is_in_range(absolute_address):
            self.irq_table.rise(IntCodes.INV_ADDRESS)
        return absolute_address

    def is_valid_frame(self, frame):
        return (frame is not None) and (frame >= 0) and (frame < self.frame_count)

    def is_valid_offset(self, offset):
        return (offset is not None) and (offset >= 0) and (offset < self.frame_size)

    def as_text(self):
        output = []
        for i in range(0, self.get_size()):
            output.append(self.memory.get_cell(i) or 0)
        output = [f'f{x//self.frame_size}: {output[x:x+self.frame_size]}'
                  for x in range(0, len(output), self.frame_size)]
        return '\n'.join(output)
