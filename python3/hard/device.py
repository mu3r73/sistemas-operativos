from abc import ABCMeta, abstractmethod

from hard.interrupt import IntCodes
from usr.logger import Logger, LogCodes


class Device(metaclass=ABCMeta):
    def __init__(self, device_id, latency, irq_table):
        self.irq_table = irq_table
        self.id = device_id
        self.latency = latency
        self.countdown = latency
        self.data = None

    def perform_io(self):
        Logger.log(LogCodes.DEV, f'I/O - dev_id: {self.id}, data: {self.data}')
        self.do_perform_io()

    @abstractmethod
    def do_perform_io(self):
        pass

    def on_tick(self):
        self.countdown -= 1
        if self.countdown == 0:
            self.countdown = self.latency
            self.perform_io()
            self.irq_table.rise(IntCodes.PROCESS_IO_OUT, self.id)


class Console(Device):
    def do_perform_io(self):
        Logger.log(LogCodes.DEV_OUT, self.data)
        print(self.data)


class DevCodes:
    # class const
    CONSOLE = 0

    devices = {
        CONSOLE: 'CONSOLE',
    }
