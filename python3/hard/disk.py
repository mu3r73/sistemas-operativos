from hard.interrupt import IntCodes
from usr.logger import Logger, LogCodes


class Disk:
    def __init__(self, size, irq_table):
        self.irq_table = irq_table
        self.size = size
        self.used = 0
        self.files = {}  # filename -> contents
        Logger.log(LogCodes.SYS_INFO, f'disk size: {size}')

    def wipe(self):
        self.files.clear()

    def get_file(self, filename):
        if filename in self.files:
            return self.files[filename]
        else:
            return self.irq_table.rise(IntCodes.NO_SUCH_FILE, filename)

    def write_file(self, filename, contents):
        size = contents.get_size()
        if hasattr(contents, 'code'):
            size += 2
        if self.get_free_space() < size:
            self.irq_table.rise(IntCodes.NO_DISK_SPACE)
        else:
            self.files[filename] = contents
            self.used += size
            Logger.log(LogCodes.DISK_ACC, f'wrote file: {filename}')
            Logger.log(LogCodes.DISK_ACC_DET, f'(size: {size})')

    def get_free_space(self):
        return self.size - self.used

    def as_text(self):
        return f'{", ".join(self.files.keys())}\nsize: {self.size}, free: {self.get_free_space()}'

    def program_as_text(self, filename):
        if filename in self.files:
            return f'{filename}, size: {self.files.get(filename).get_size()}'
        else:
            return 'no such file'
