from hard.cpu_instruction_set import Instruction
from usr.logger import Logger, LogCodes


class CPU:
    def __init__(self, irq_table):
        self.memory_manager = None
        self.irq_table = irq_table
        # registers
        self.pc = -1
        self.ir = None
        self.reg_a = 0
        self.reg_b = 0
        self.reg_x = 0
        # self.in_kernel_mode = True

    def set_memory_manager(self, memory_manager):
        self.memory_manager = memory_manager

    def is_idle(self):
        return self.pc == -1

    def on_tick(self):
        # if (not self.in_kernel_mode) and (not self.is_idle()):
        if not self.is_idle():
            self.fetch_decode_execute()
        self.log_status()

    def fetch_decode_execute(self):
        self.decode(self.fetch())
        self.fetch_operand()
        self.increment_pc()
        self.execute()

    def fetch(self):
        return self.memory_manager.get_cell(self.pc)

    def decode(self, instr):
        self.ir = Instruction.decode(self.pc, instr, self)

    def fetch_operand(self):
        self.ir.fetch_operand()

    def increment_pc(self):
        self.pc += self.ir.size

    def execute(self):
        self.ir.execute()

    def as_text(self):
        return f'PC: {self.pc}, A: {self.reg_a}, B: {self.reg_b}, X: {self.reg_x}'

    def log_status(self):
        Logger.log(LogCodes.CPU_PC, f'{(self.pc or 0):#06x}')
        Logger.log(LogCodes.CPU_REG_A, f'{(self.reg_a or 0):#06x}')
        Logger.log(LogCodes.CPU_REG_B, f'{(self.reg_b or 0):#06x}')
        Logger.log(LogCodes.CPU_REG_X, f'{(self.reg_x or 0):#06x}')
