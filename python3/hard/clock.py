import time

try:
    from PyQt4 import QtCore
    from PyQt4.QtCore import pyqtSignal as Signal
except ImportError:
    from PySide import QtCore
    from PySide.QtCore import Signal

from usr.logger import Logger, LogCodes


class Clock:
    def __init__(self, freq_s):
        self.ticks = 0
        self.hardware = None
        self.os_clock = None
        self.ui_clock = None
        self.thread = None
        self.setup_thread(freq_s)
        Logger.log(LogCodes.SYS_INFO, f'clock frequency: {(1 / freq_s):.2f} Hz')

    def set_hardware(self, hardware):
        self.hardware = hardware

    def set_os_clock(self, os_clock):
        self.os_clock = os_clock

    def set_ui_clock(self, ui_clock):
        self.ui_clock = ui_clock

    def start_tick_loop(self):
        self.resume()

    def tick_once(self):
        self.ticks += 1
        Logger.log(LogCodes.CLK, self.ticks)
        Clock.send_tick_to(self.ui_clock)
        self.hardware.on_tick()
        Clock.send_tick_to(self.os_clock)

    @staticmethod
    def send_tick_to(clock):
        if clock:
            clock.on_tick()

    def setup_thread(self, freq_s):
        self.thread = ClockThread(freq_s, self.tick_once)
        self.thread.start()

    def pause(self):
        self.thread.ticking = False

    def resume(self):
        self.thread.ticking = True

    def stop(self):
        self.pause()


class ClockThread(QtCore.QThread):
    task = Signal()

    # noinspection PyUnresolvedReferences
    def __init__(self, freq_s, clock_task):
        super().__init__()
        self.freq_s = freq_s  # real
        self.task.connect(clock_task)
        self.ticking = False

    # noinspection PyUnresolvedReferences
    def run(self):
        while True:
            if self.ticking:
                self.task.emit()
            else:
                time.sleep(self.freq_s / 100)
                continue
            time.sleep(self.freq_s)
