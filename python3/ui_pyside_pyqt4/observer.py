try:
    from PyQt4 import QtGui
    from PyQt4.QtCore import Qt, QThread, pyqtSignal as Signal
except ImportError:
    from PySide import QtGui
    from PySide.QtCore import Qt, QThread, Signal


class Observer(QThread):
    update = Signal(str)

    def __init__(self, text_edit, append):
        super().__init__()
        self.text_edit = text_edit
        self.append = append

    # noinspection PyUnresolvedReferences
    def on_text_received(self, txt):
        self.update.emit(txt)

    def update_control(self, txt):
        if self.append:
            self.text_edit.appendPlainText(txt)
        else:
            self.text_edit.setPlainText(txt)

    # noinspection PyUnresolvedReferences,PyMethodOverriding
    def start(self):
        super().start()
        self.update.connect(self.update_control)
