from abc import abstractmethod

try:
    from PyQt4 import QtCore, QtGui
    from PyQt4.QtCore import pyqtSignal as Signal
except ImportError:
    from PySide import QtCore, QtGui
    from PySide.QtCore import Signal

from usr.logger import Logger, LogCodes
from ui_pyside_pyqt4.observer import Observer
from ui_pyside_pyqt4.controller import Controller


# custom windows

class Window(QtGui.QWidget):
    def __init__(self, title, height, width):
        super().__init__()
        self.setWindowTitle(title)
        self.setFixedSize(height, width)

        self.add_controls()

    @abstractmethod
    def add_controls(self):
        pass

    @staticmethod
    def add_observer(text_edit, log_cats, append):
        obs = Observer(text_edit, append)
        obs.start()
        for cat in log_cats:
            Logger.add_observer(cat, obs)


class UIConsole(Window):
    def __init__(self, kernel):
        super().__init__('console', 640, 480)
        self.kernel = kernel
        self.ctrl = Controller(kernel)
        # turn clock off
        self.ctrl.toggle_clock(True)

        self.internals = UIInternals(self)
        self.internals.show()
        self.show()

    # noinspection PyAttributeOutsideInit
    def add_controls(self):
        # output
        self.lbl_output = QtGui.QLabel('output:', self)
        self.lbl_output.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.te_output = ROPlainTextEdit('', self)
        Window.add_observer(self.te_output, [LogCodes.DEV_OUT, LogCodes.SHELL, LogCodes.STD_ERR], True)

        # command line
        self.lbl_command = QtGui.QLabel('command:', self)
        self.lbl_command.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.le_command = LineEdit(self)

        # place controls
        self.lbl_output.setGeometry(QtCore.QRect(10, 20, 62, 18))
        self.te_output.setGeometry(QtCore.QRect(20, 45, 601, 371))

        self.lbl_command.setGeometry(QtCore.QRect(10, 435, 81, 18))
        self.le_command.setGeometry(QtCore.QRect(100, 430, 521, 28))

        # attach handlers
        self.attach_handlers()

        # set focus
        self.le_command.setFocus()

    # noinspection PyUnresolvedReferences
    def attach_handlers(self):
        self.le_command.key_up_pressed.connect(self.prev_command)
        self.le_command.key_down_pressed.connect(self.next_command)
        self.le_command.returnPressed.connect(self.run_command)

    def prev_command(self):
        self.le_command.setText(self.ctrl.get_prev_command())

    def next_command(self):
        self.le_command.setText(self.ctrl.get_next_command())

    def run_command(self):
        self.ctrl.run_command(self.le_command.text())
        self.le_command.clear()

    def closeEvent(self, event):
        self.ctrl.shutdown()
        event.accept()


class UIInternals(Window):
    def __init__(self, main):
        self.main = main
        super().__init__('internals', 1120, 480)

    # noinspection PyAttributeOutsideInit
    def add_controls(self):
        # clock
        self.cb_freeze_clock = QtGui.QCheckBox('freeze clock', self)
        self.cb_freeze_clock.setChecked(True)
        self.lbl_clock = QtGui.QLabel('clock:', self)
        self.te_clock = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_clock, [LogCodes.CLK], False)
        self.btn_tick_once = QtGui.QPushButton('tick', self)
        self.btn_tick_once.setVisible(True)

        # cpu registers
        self.lbl_cpu_regs = QtGui.QLabel('CPU registers:', self)

        self.lbl_reg_a = QtGui.QLabel('A:', self)
        self.te_reg_a = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_reg_a, [LogCodes.CPU_REG_A], False)

        self.lbl_reg_b = QtGui.QLabel('B:', self)
        self.te_reg_b = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_reg_b, [LogCodes.CPU_REG_B], False)

        self.lbl_reg_x = QtGui.QLabel('X:', self)
        self.te_reg_x = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_reg_x, [LogCodes.CPU_REG_X], False)

        self.lbl_pc = QtGui.QLabel('PC:', self)
        self.te_pc = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_pc, [LogCodes.CPU_PC], False)

        # current instruction
        self.lbl_instruction = QtGui.QLabel('current instruction:', self)
        self.te_instruction = ROLinePlainTextEdit('', self)
        Window.add_observer(self.te_instruction, [LogCodes.INSTR], False)

        # pcb table statistics
        self.lbl_pcb_stats = QtGui.QLabel('statistics:', self)
        self.te_pcb_stats = ROPlainTextEdit('', self)
        Window.add_observer(self.te_pcb_stats, [LogCodes.STATS], False)

        # Gantt
        self.lbl_gantt = QtGui.QLabel('Gantt:', self)
        self.te_gantt = ROTextEditWithHorizontalAutoScroll('', self)
        Window.add_observer(self.te_gantt, [LogCodes.GANTT], False)

        # memory
        self.lbl_mem = QtGui.QLabel('memory:', self)
        self.te_mem = ROPlainTextEdit('', self)
        Window.add_observer(self.te_mem, [LogCodes.MEM_DUMP], False)

        # program
        self.lbl_program = QtGui.QLabel('program:', self)
        self.te_program = ROPlainTextEdit('', self)
        Window.add_observer(self.te_program, [LogCodes.DISPATCH_SWITCH], False)

        # place controls
        self.cb_freeze_clock.setGeometry(QtCore.QRect(70, 20, 121, 23))
        self.lbl_clock.setGeometry(QtCore.QRect(20, 52, 62, 18))
        self.te_clock.setGeometry(QtCore.QRect(70, 49, 151, 28))
        self.btn_tick_once.setGeometry(QtCore.QRect(230, 50, 91, 26))

        self.lbl_cpu_regs.setGeometry(QtCore.QRect(20, 100, 111, 18))
        self.lbl_reg_a.setGeometry(QtCore.QRect(40, 130, 21, 20))
        self.te_reg_a.setGeometry(QtCore.QRect(70, 125, 91, 28))
        self.lbl_reg_b.setGeometry(QtCore.QRect(40, 160, 21, 20))
        self.te_reg_b.setGeometry(QtCore.QRect(70, 155, 91, 28))
        self.lbl_reg_x.setGeometry(QtCore.QRect(200, 130, 21, 20))
        self.te_reg_x.setGeometry(QtCore.QRect(230, 125, 91, 28))
        self.lbl_pc.setGeometry(QtCore.QRect(190, 160, 31, 20))
        self.te_pc.setGeometry(QtCore.QRect(230, 155, 91, 28))

        self.lbl_instruction.setGeometry(QtCore.QRect(20, 210, 141, 18))
        self.te_instruction.setGeometry(QtCore.QRect(160, 205, 161, 28))

        self.lbl_pcb_stats.setGeometry(QtCore.QRect(350, 20, 160, 18))
        self.te_pcb_stats.setGeometry(QtCore.QRect(350, 45, 491, 226))

        self.lbl_gantt.setGeometry(QtCore.QRect(350, 280, 62, 18))
        self.te_gantt.setGeometry(QtCore.QRect(350, 304, 491, 156))

        self.lbl_mem.setGeometry(QtCore.QRect(870, 20, 81, 18))
        self.te_mem.setGeometry(QtCore.QRect(870, 45, 231, 417))

        self.lbl_program.setGeometry(QtCore.QRect(20, 245, 80, 18))
        self.te_program.setGeometry(QtCore.QRect(20, 270, 300, 190))

        # attach handlers
        self.attach_handlers()

        # set focus
        self.cb_freeze_clock.setFocus()

    # noinspection PyUnresolvedReferences
    def attach_handlers(self):
        self.btn_tick_once.clicked.connect(self.clock_tick_once)
        self.cb_freeze_clock.stateChanged.connect(self.toggle_clock)

    def clock_tick_once(self):
        self.main.ctrl.clock_tick_once()

    def toggle_clock(self):
        self.btn_tick_once.setVisible(self.sender().isChecked())
        self.main.ctrl.toggle_clock(self.sender().isChecked())


# custom controls

class LineEdit(QtGui.QLineEdit):
    key_up_pressed = Signal()
    key_down_pressed = Signal()

    # noinspection PyUnresolvedReferences
    def keyPressEvent(self, event):
        super().keyPressEvent(event)
        if event.key() == QtCore.Qt.Key_Up:
            self.key_up_pressed.emit()
        elif event.key() == QtCore.Qt.Key_Down:
            self.key_down_pressed.emit()


class ROPlainTextEdit(QtGui.QPlainTextEdit):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.setReadOnly(True)


class ROLinePlainTextEdit(ROPlainTextEdit):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)


class ROTextEditWithHorizontalAutoScroll(QtGui.QTextEdit):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.setReadOnly(True)
        self.setCurrentFont(QtGui.QFont('Monospace'))
        self.setLineWrapMode(QtGui.QTextEdit.NoWrap)

    def setPlainText(self, text):
        super().setPlainText(text)
        self.moveCursor(QtGui.QTextCursor.EndOfLine)
        self.setCurrentFont(QtGui.QFont('Monospace'))
