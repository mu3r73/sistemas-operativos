import os
import signal

from usr.gantt import Gantt
from soft.timer import SoftClock


class Controller:
    def __init__(self, kernel):
        self.kernel = kernel
        self.cmd_history = []
        self.cmd_pointer = 0
        self.ui_clock = None
        self.gantt = None
        self.mk_clock()
        self.mk_gantt()

    def clock_tick_once(self):
        self.kernel.hardware.clock.tick_once()

    def toggle_clock(self, paused):
        if paused:
            self.kernel.hardware.clock.pause()
        else:
            self.kernel.hardware.clock.resume()

    def run_command(self, cmd_line):
        if cmd_line in self.cmd_history:
            self.cmd_history.remove(cmd_line)
        self.cmd_history.append(cmd_line)
        self.cmd_pointer = len(self.cmd_history)
        self.kernel.shell.run(cmd_line)

    def get_prev_command(self):
        if self.cmd_pointer > 0:
            self.cmd_pointer -= 1
        if self.cmd_history:
            return self.cmd_history[self.cmd_pointer]
        else:
            return ''

    def get_next_command(self):
        history_len = len(self.cmd_history)
        if self.cmd_pointer < (history_len - 1):
            self.cmd_pointer += 1
            return self.cmd_history[self.cmd_pointer]
        elif self.cmd_pointer == (history_len - 1):
            return ''

    def mk_clock(self):
        self.ui_clock = SoftClock()
        self.kernel.hardware.clock.set_ui_clock(self.ui_clock)

    def mk_gantt(self):
        self.gantt = Gantt(self.kernel.pcb_table)
        self.connect_to_ui_clock(self.gantt)

    def connect_to_ui_clock(self, component):
        self.ui_clock.connect(component)

    def shutdown(self):
        self.kernel.hardware.clock.stop()
        os.kill(os.getpid(), signal.SIGTERM)
        # os.kill(os.getpid(), signal.SIGKILL) # breaks on Windows
        # os.exit() # doesn't kill threads
