from hard.cpu import CPU
from hard.cpu_instruction_set import InstrCodes
from hard.memory import Memory


class Program:
    def __init__(self, start_at, param_count, code):
        self.start_at = start_at
        self.param_count = param_count
        self.code = code

    def get_size(self):
        return len(self.code)

    def as_text(self, columns):
        output = []
        for x in self.code:
            output.append(x or 0)
        output = [f'{x//columns}: {output[x:x+columns]}' for x in range(0, len(output), columns)]
        return '\n'.join(output)

    def as_source(self):
        size = len(self.code)
        pseudo_mem = Memory(size)
        for i in range(0, size):
            pseudo_mem.set_cell(i, self.code[i])

        pseudo_cpu = CPU(None)
        pseudo_cpu.set_memory_manager(pseudo_mem)
        pseudo_cpu.pc = self.start_at

        output = []
        while pseudo_cpu.pc < size:
            pseudo_cpu.decode(pseudo_cpu.fetch())
            pseudo_cpu.fetch_operand()
            output.append(pseudo_cpu.ir.as_text(True))
            if pseudo_cpu.ir.code == InstrCodes.END:
                break
            pseudo_cpu.increment_pc()
        return '\n'.join(output)
