class LogCodes:
    # class const
    FIRST_SOFT_CAT = 18

    CLK = 0
    CPU_PC = 1
    CPU_REG_A = 2
    CPU_REG_B = 3
    CPU_REG_X = 4
    IRQ = 5
    IRQ_DET = 6
    MEM_ACC = 7
    MEM_ACC_DET = 8
    MEM_DUMP = 9
    MMU_BASE_ADDRESS = 10
    INSTR = 11
    INSTR_DET = 12
    DISK_ACC = 13
    DISK_ACC_DET = 14
    DEV = 15
    DEV_DET = 16
    DEV_OUT = 17
    HANDLER = 18
    HANDLER_DET = 19
    PROGRAM_LDR = 20
    PROGRAM_LDR_DET = 21
    DISPATCH = 22
    DISPATCH_DET = 23
    DISPATCH_SWITCH = 24
    SCHEDULE = 25
    SCHEDULE_DET = 26
    PCB_TABLE = 27
    PCB_TABLE_DET = 28
    DEV_MAN = 29
    LOGICAL_DEV = 30
    LOGICAL_DEV_DET = 31
    SHELL = 32
    STD_ERR = 33
    STATS = 34
    GANTT = 35
    SYS_INFO = 98
    ALL = 99

    cats = {
        CLK: 'CLK',
        CPU_PC: 'CPU_PC',
        CPU_REG_A: 'CPU_REG_A',
        CPU_REG_B: 'CPU_REG_B',
        CPU_REG_X: 'CPU_REG_X',
        IRQ: 'IRQ',
        IRQ_DET: 'IRQ_DET',
        MMU_BASE_ADDRESS: 'MMU_BASE_ADDRESS',
        MEM_ACC: 'MEM_ACC',
        MEM_ACC_DET: 'MEM_ACC_DET',
        MEM_DUMP: 'MEM_DUMP',
        INSTR: 'INSTR',
        INSTR_DET: 'INSTR_DET',
        DISK_ACC: 'DISK_ACC',
        DISK_ACC_DET: 'DISK_ACC_DET',
        DEV: 'DEV',
        DEV_DET: 'DEV_DET',
        DEV_OUT: 'DEV_OUT',
        HANDLER: 'HANDLER',
        HANDLER_DET: 'HANDLER_DET',
        PROGRAM_LDR: 'PROGRAM_LDR',
        PROGRAM_LDR_DET: 'PROGRAM_LDR_DET',
        DISPATCH: 'DISPATCH',
        DISPATCH_DET: 'DISPATCH_DET',
        DISPATCH_SWITCH: 'DISPATCH_SWITCH',
        SCHEDULE: 'SCHEDULE',
        SCHEDULE_DET: 'SCHEDULE_DET',
        PCB_TABLE: 'PCB_TABLE',
        PCB_TABLE_DET: 'PCB_TABLE_DET',
        DEV_MAN: 'DEV_MAN',
        LOGICAL_DEV: 'LOGICAL_DEV',
        LOGICAL_DEV_DET: 'LOGICAL_DEV_DET',
        SHELL: 'SHELL',
        STD_ERR: 'STD_ERR',
        STATS: 'STATS',
        GANTT: 'GANTT',
        SYS_INFO: 'SYS_INFO',
        ALL: 'ALL',
    }


class Logger:
    # class vars
    cats_ignored = []
    observers = {}  # log_code -> observer

    @staticmethod
    def dont_log_cat(cat):
        Logger.cats_ignored.append(cat)

    @staticmethod
    def add_observer(cat, observer):
        if cat == LogCodes.ALL:
            for lc in LogCodes.cats.keys():
                Logger.observers[lc] = observer
        else:
            Logger.observers[cat] = observer

    @staticmethod
    def log(cat, val):
        txt = str(val)
        if cat in Logger.cats_ignored:
            return

        observer = Logger.observers.get(cat)
        if observer:
            observer.on_text_received(txt)
        else:
            txt = f'{LogCodes.cats[cat]}: {txt}'
            if cat < LogCodes.FIRST_SOFT_CAT:  # hardware
                txt = '  ' + txt
            print(txt)
