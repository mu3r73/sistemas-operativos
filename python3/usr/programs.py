from hard.cpu_instruction_set import InstrCodes
from usr.program import Program


# all programs (c) Román, translated from Java by N

class Programs:
    # class vars
    prog_idle = Program(
        start_at=0,
        param_count=0,
        code=[
            InstrCodes.LDX, 1,
            InstrCodes.NOP,
            InstrCodes.JXNZ, -3,
        ],
    )

    prog_sum = Program(
        start_at=2,
        param_count=2,
        code=[
            InstrCodes.NOP, InstrCodes.NOP,  # params
            InstrCodes.LDAm, 0,
            InstrCodes.LDBm, 1,
            InstrCodes.ADD,
            InstrCodes.PRNTA,
            InstrCodes.END,
        ],
    )

    prog_mul = Program(
        start_at=2,
        param_count=2,
        code=[
            InstrCodes.NOP, InstrCodes.NOP,  # params
            InstrCodes.LDA, 0,
            InstrCodes.LDBm, 0,
            InstrCodes.LDXm, 1,
            InstrCodes.ADD,
            InstrCodes.DECX,
            InstrCodes.JXNZ, -4,
            InstrCodes.PRNTA,
            InstrCodes.END,
        ],
    )

    # calc inf seq of Fib numbers
    prog_fibs = Program(
        start_at=2,
        param_count=0,
        code=[
            0, 1,  # data: elements n-2 and n-1 of the seq
            InstrCodes.LDX, 1,
            InstrCodes.LDAm, 0,  # elem 1 in the seq
            InstrCodes.PRNTA,
            InstrCodes.LDAm, 1,  # elem 2 in the seq
            InstrCodes.PRNTA,
            InstrCodes.LDAm, 0,  # (loop) load elem n-2 of the seq
            InstrCodes.LDBm, 1,  # load elem n-1 of the seq
            InstrCodes.ADD,  # new element
            InstrCodes.PRNTA,
            InstrCodes.STA, 1,  # store new elem as n-1
            InstrCodes.STB, 0,  # store prev elem as n-2
            InstrCodes.JXNZ, -12  # jump to (loop)
        ],
    )

    # calculate the sum of 1 + 2 + 3 + ... + n, showing partial results
    prog_sum_from_1_to_n = Program(
        start_at=1,
        param_count=1,
        code=[
            InstrCodes.NOP,  # param & temp var
            InstrCodes.LDXm, 0,
            InstrCodes.LDA, 0,
            InstrCodes.LDBm, 0,  # (loop)
            InstrCodes.ADD,
            InstrCodes.PRNTA,
            InstrCodes.DECX,
            InstrCodes.STX, 0,
            InstrCodes.JXNZ, -9,  # jump to (loop)
            InstrCodes.END,
        ]
    )

    # calc fact n
    prog_fact = Program(
        start_at=2,
        param_count=1,
        code=[
            InstrCodes.NOP, 1,  # parameter 1 & accumulator for result
            InstrCodes.LDXm, 0,  # special case: 1!
            InstrCodes.DECX,
            InstrCodes.STX, 0,
            InstrCodes.JXNZ, 4,
            InstrCodes.LDA, 1,
            InstrCodes.PRNTA,
            InstrCodes.END,
            # mul
            InstrCodes.LDA, 0,  # clear result
            InstrCodes.LDBm, 1,  # accumulator x ...
            InstrCodes.LDXm, 0,  # ... x param
            InstrCodes.ADD,  # (loop)
            InstrCodes.DECX,
            InstrCodes.JXNZ, -4,  # jump to (loop)
            # add to accumulator
            InstrCodes.LDBm, 1,  # accumulator += result of mul
            InstrCodes.ADD,
            InstrCodes.STA, 1,
            # dec param
            InstrCodes.LDXm, 0,  # param -= 1
            InstrCodes.DECX,
            InstrCodes.STX, 0,
            # repeat
            InstrCodes.JXNZ, -22,
            InstrCodes.LDAm, 1,  # otherwise, end
            InstrCodes.PRNTA,
            InstrCodes.END,
        ]
    )
