This is my OSs Homewyrk.

Requirements:
- Python 3.6+ (f-strings / string interpolation)
- PyQt4 or PySide (GUI)


---

PySide/PyQt4 on Windows:

pip install pyside/pyqt4 is NOT working (2017/09/25), but pip can install PySide and/or PyQt4 from a(n unofficial) wheel package.
In addition, you will need to install Visual C++ Redistributable for Visual Studio 2015.

1- Download the pyside wheel (http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyside or the pyqt4 wheel (http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyqt4).

Note: make sure you download the right version for your system (win32 or win_amd64) and 'cp36-cp36m' for python 3.6.

For example, on my 32-bit Windows vm, I downloaded:
PySide‑1.2.4‑cp36‑cp36m‑win32.whl
-or-
PyQt4‑4.11.4‑cp36‑cp36m‑win32.whl

2- Install the wheel package using pip.

For example, to install the package(s) globally (for all users):
a) open a command prompt in administrator mode
b) go to the folder where you downloaded the wheel package(s)
c) enter the command to install the package(s) - for example:
pip install PySide‑1.2.4‑cp36‑cp36m‑win32.whl
-or-
pip install PyQt4‑4.11.4‑cp36‑cp36m‑win32.whl

3- If you don't have it already, download and install Visual C++ Redistributable for Visual Studio 2015 (https://www.microsoft.com/en-us/download/details.aspx?id=48145)

